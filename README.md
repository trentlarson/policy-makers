# Policy Makers

Evolution of policies via voluntary creation, adoption, and certification


### Create a policy (anyone)

Summary: create a merge request with a new file into this repository.  It will be merged by our site managers.

1. Choose an existing policy or template
  ![](image/policy-1.png "")
1. Choose to display source
  ![](image/policy-2.png "")
1. Copy the file contents
  ![](image/policy-3.png "")
1. Go to the directory where you want to insert the new policy
  ![](image/policy-4.png "")
1. Create a new file
  ![](image/policy-5.png "")
1. Paste the file contents and edit any details
  ![](image/policy-6.png "")
1. Give it a file name, preferably with ".md" on the end
  ![](image/policy-7.png "")
1. At the bottom of the page, write any message, choose "master" branch, and click "Commit Changes"
  ![](image/policy-8.png "")
1. At the top of the page, click "Create merge request"
  ![](image/policy-9.png "")
1. Select from your "/policy-makers:master" to "trentlarson/policy-makers:master", then click "Compare branches and continue"
  ![](image/policy-10.png "")
1. Fill out any descriptive info, then go to the bottom of the page and click "Submit merge request"
  ![](image/policy-11.png "")
1. Success!
  ![](image/policy-12.png "")

Now the managers of this site will review your submission and publish it.  Feel free to raise [an issue](https://gitlab.com/trentlarson/policy-makers/-/issues) if you need help.


### Commit to a policy (anyone)

- Merge request?  Endorser?

### Declare yourself a policy expert (anyone)

- Merge request?  Endorser?

### Publicize a policy expert (owners of this site)

- Managers of this site will confirm experts, with their justifications/credentials.
